from __future__ import print_function

import contextlib
import os
import shlex
import shutil
import sys
import tempfile
from collections import namedtuple
import json
from shutil import copy2
from subprocess import Popen, PIPE
import stat

import collections

from .errors import ValidationError
from .log import log

home = os.path.expanduser('~')

system_files_ext = {'.DS_Store', 'Thumbs.db'}


def add_env_var(value, param='PATH'):
    if value not in os.environ.get(param, ''):
        if not value:
            os.environ[param] = value
        else:
            os.environ[param] += os.pathsep + value


def ensure_dir_exists(full_path):
    if not os.path.exists(full_path):
        cmd = "mkdir -p '{path}'".format(path=full_path.encode('utf-8'))
        # log.error(cmd)
        os.system(cmd)
        return False
    return True


def remove_path(path):
    if os.path.isdir(path) and not os.path.islink(path):
        shutil.rmtree(path)
    elif os.path.exists(path):
        os.remove(path)


def del_empty_dirs(path):
    empty = True

    for target in os.listdir(path):
        p = os.path.join(path, target)
        if os.path.isdir(p):
            if not del_empty_dirs(p):
                empty = False
        else:
            empty = False

    if empty:
        print('del: %s' % path)
        os.rmdir(path)

    return empty


def ensure_dir_exists_for_file(path):
    dirname, _ = os.path.split(path)
    return ensure_dir_exists(dirname)


def move_file(src, dst):
    ensure_dir_exists_for_file(dst)
    cmd = "mv '{src}' '{dst}'".format(src=src, dst=dst)
    os.system(cmd)


def copy_file(src, dst, mkdir=True):
    if mkdir:
        ensure_dir_exists_for_file(dst)

    cmd = "cp '{src}' '{dst}'".format(src=src, dst=dst)
    os.system(cmd)


def copy_path(src, dst):
    ensure_dir_exists(dst)
    cmd = "cp -r '{src}' '{dst}'".format(src=src, dst=dst)
    os.system(cmd)


def create_symlink(src, dst):
    ensure_dir_exists_for_file(dst)
    os.symlink(src, dst)


def copy_files(files, src, dst, remove_src=False):
    """
    copy list files recursively(cp -r)
    :param files: Iterator[str] relative filenames
    :param src: str. source path
    :param dst: str. destination path
    :param remove_src: remove_source files(aka mv)
    :return: is any file updated
    """
    updated = False

    for fname in files:
        src_fullname = os.path.join(src, fname)
        dst_fullname = os.path.join(dst, fname)

        ensure_dir_exists_for_file(dst_fullname)
        updated |= copy_if_modified(src_fullname, dst_fullname)
        if remove_src:
            os.remove(src_fullname)

    return updated


def copy_if_modified(src, dst):
    """

    :param src: source path
    :param dst: destination path
    :return: is file updated
    """
    if not os.path.exists(dst) or os.stat(src).st_mtime - os.stat(
            dst).st_mtime > 1:
        copy2(src, dst)
        return True
    return False


def find_files(path, endswith=None):
    if endswith:
        for root, dirs, fnames in os.walk(path):
            for fname in fnames:
                if fname in system_files_ext:
                    continue
                if fname.endswith(endswith):
                    yield fname, os.path.join(root, fname)

    else:
        for root, dirs, fnames in os.walk(path):
            for fname in fnames:
                if fname in system_files_ext:
                    continue
                yield fname, os.path.join(root, fname)


def read_json(fname):
    from utils.log import log
    try:
        with open(fname) as f:
            return json.load(f)
    except IOError as e:
        log.critical("File '%s' doesn't exist" % fname)
        raise e
    except ValueError as e:
        log.critical("File '%s' is not valid json" % fname)
        raise e


def dump_json(data, fullname, *args, **kwargs):
    if fullname.endswith(os.sep):
        raise ValueError('File path expected. not dir: {}'.format(fullname))

    kwargs.setdefault('indent', 4)
    kwargs.setdefault('sort_keys', True)
    s = json.dumps(data, *args, **kwargs)
    write_to_file_if_changed(s, fullname)


def write_to_file_if_changed(content, fname):
    old_content = None
    if os.path.exists(fname) and os.path.isfile(fname):
        with open(fname) as f:
            old_content = f.read()

    ensure_dir_exists_for_file(fname)

    if old_content != content:
        with open(fname, 'w') as out:
            out.write(content)
            return True
    return False


CallResult = namedtuple('CallResult', 'code,output,stderr,is_ok')


def exec_cmd(cmd, shell=False, without_split=False):
    from utils.log import log
    from utils.cli import add_system_envs

    log.debug("command: " + str(cmd))
    add_system_envs()

    # todo use six.string_types
    if isinstance(cmd, basestring) and not without_split:
        cmd = shlex.split(cmd)

    p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=shell)
    output, stderr = p.communicate()
    log.debug("output: " + output)

    is_ok = not p.returncode
    if not is_ok:
        log.error("command failed: {}".format(cmd))
        log.error("output: '{}'".format(output))
        log.error("stderr: '{}'".format(stderr))

    return CallResult(
        code=p.returncode, output=output, stderr=stderr, is_ok=is_ok)


@contextlib.contextmanager
def cd(newdir, cleanup=lambda: True):
    try:
        prevdir = os.getcwd()
    except:
        os.chdir(home)
        prevdir = home

    try:
        os.chdir(newdir)
        yield
    finally:
        os.chdir(prevdir)
        cleanup()


@contextlib.contextmanager
def tempdir():
    """
    with tempdir() as dirpath:
        pass # do something here
    """
    dirpath = tempfile.mkdtemp()

    def cleanup():
        shutil.rmtree(dirpath)

    with cd(dirpath, cleanup):
        yield dirpath


def get_file_size(path):
    return os.stat(path).st_size


def is_filename_valid(full_name):
    try:
        full_name = full_name.decode('ascii')
        if ' ' in full_name:
            log.error('Spaces are denied: "{}"'.format(full_name))
            return False
    except (UnicodeDecodeError, UnicodeEncodeError):
        log.error("Bad name(non ascii symbols)")
        return False
    return True


def validate_filenames(root):
    if not all(
            is_filename_valid(full_name) for _, full_name in find_files(root)):
        raise ValidationError("validation failed")


def rsync(src,
          dst,
          username,
          hostname=None,
          delete_before=False,
          verbose=True,
          exclude=(),
          archive=True,
          checksum=True):
    # todo use six.string_types
    if not isinstance(exclude, collections.Iterable):
        raise TypeError("'exclude' must be iterable {}".format(exclude))
    if isinstance(exclude, basestring):
        raise TypeError("'exclude' can't be string {}".format(exclude))

    excl = ' '.join('--exclude=' + s for s in set(exclude) | system_files_ext)

    if hostname:  # remote
        dst_path = "{user}@{host}:{path}".format(
            user=username, host=hostname, path=dst)
    else:  # local
        dst_path = dst

    delete_flag = '--delete-before' if delete_before else ''
    verbose_flag = '--verbose' if verbose else ''
    checksum_flag = '--checksum' if checksum else ''
    archive_flag = '--archive' if archive else ''

    cmd = "rsync {checksum_flag} {archive_flag} {delete_flag} " \
          "{verbose} {exclude} {src}/ {dst}".format(
        src=src, dst=dst_path,
        checksum_flag=checksum_flag, archive_flag=archive_flag,
        exclude=excl, verbose=verbose_flag, delete_flag=delete_flag)
    os.system(cmd)


def get_current_platform():
    if sys.platform in ('linux', 'linux2'):
        return 'linux'
    if sys.platform == 'darwin':
        return 'mac'
    raise OSError("Unsupported platform %s" % sys.platform)


def get_user():
    from getpass import getuser
    return getuser()


def make_executable(path):
    assert os.path.exists(path), "Path not found: {}".format(path)
    st = os.stat(path)
    os.chmod(path, st.st_mode | stat.S_IEXEC)
