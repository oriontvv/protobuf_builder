import progressbar

_enable_progressbar = True

_bar = None


class ProgressbarProxy(progressbar.ProgressBar):
    def update(self, *args, **kwargs):
        pass

    def start(self, max_value=None):
        pass

    def finish(self):
        pass


def init_propressbar(enable):
    global _enable_progressbar
    _enable_progressbar = enable


def get_progressbar(*args, **kwargs):
    global _bar
    if _bar is None:
        if _enable_progressbar:
            _bar = progressbar.ProgressBar(*args, **kwargs)
        else:
            _bar = ProgressbarProxy()
    assert _bar, "Bad _bad"
    return _bar
