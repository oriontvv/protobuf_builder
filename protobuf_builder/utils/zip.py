import os
from StringIO import StringIO
import zipfile


class InMemoryZip(object):
    def __init__(self):
        # Create the in-memory file-like object
        self.in_memory_zip = StringIO()

    def _write(self, zf, arcname):
        """Put the bytes from filename into the archive under the name
        arcname."""
        if not zf.fp:
            raise RuntimeError(
                "Attempt to write to ZIP archive that was already closed")

        # Create ZipInfo instance to store file information
        arcname = os.path.normpath(os.path.splitdrive(arcname)[1])
        while arcname[0] in (os.sep, os.altsep):
            arcname = arcname[1:]
        arcname += '/'
        zinfo = zipfile.ZipInfo(arcname)

        if zinfo.filename in zf.NameToInfo:
            # duplicate fname in zip
            return

        zinfo.external_attr = 16877 << 16L  # Unix attributes for directory
        zinfo.compress_type = zipfile.ZIP_STORED

        zinfo.file_size = 0
        zinfo.flag_bits = 0x00
        zinfo.header_offset = zf.fp.tell()  # Start of header bytes

        zf._writecheck(zinfo)
        zf._didModify = True

        zinfo.file_size = 0
        zinfo.compress_size = 0
        zinfo.CRC = 0
        zinfo.external_attr |= 0x10  # MS-DOS directory flag
        zf.filelist.append(zinfo)
        zf.NameToInfo[zinfo.filename] = zinfo
        zf.fp.write(zinfo.FileHeader(False))

    def append_dir(self, dirname):
        zf = zipfile.ZipFile(self.in_memory_zip, "a", zipfile.ZIP_DEFLATED,
                             False)

        self._write(zf, dirname)
        return self

    def append_dirs_for_file(self, filepath):
        def _append_dir_rec(path):
            path, fname = os.path.split(path)
            if path:
                _append_dir_rec(path)
                self.append_dir(path)

        _append_dir_rec(filepath)

    def append_file(self, filename_in_zip, file_content, encoding='utf-8'):
        """Appends a file with name filename_in_zip and contents of
        file_contents to the in-memory zip."""
        # Get a handle to the in-memory zip in append mode
        zf = zipfile.ZipFile(self.in_memory_zip, "a", zipfile.ZIP_DEFLATED,
                             False)

        # Try encoding
        if encoding:
            file_content = file_content.encode(encoding)

        # Write the file to the in-memory zip
        zf.writestr(filename_in_zip, file_content)

        # Mark the files as having been created on Windows so that
        # Unix permissions are not inferred as 0000
        for zfile in zf.filelist:
            zfile.create_system = 0

        return self

    def read(self):
        """Returns a string with the contents of the in-memory zip."""
        self.in_memory_zip.seek(0)
        return self.in_memory_zip.read()

    def write_to_file(self, filename):
        """Writes the in-memory zip to a file."""
        f = file(filename, "w")
        f.write(self.read())
        f.close()


if __name__ == "__main__":
    # Run a test
    imz = InMemoryZip()
    imz.append_file("test.txt", "Another test")
    imz.append_file("test2.txt", "Still another")
    imz.append_file("subdir/test3.txt", "It's works too!")

    imz.write_to_file("test.zip")
