# coding: utf-8

import os
import struct
import zlib
import shutil

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from .log import log


class UnknownImageFormat(Exception):
    pass


"""
    struct module format description:
	c:  char
    h:  int16
    H:  uint16
    i:  int32
    I:  uint32
    <:  little-endian
    >:	big-endian
"""


class AbstractImageFormat(object):
    def match(self, data, size, f):
        raise NotImplementedError()

    def read_size(self, data, f):
        raise NotImplementedError()


class Png(AbstractImageFormat):
    def match(self, data, size, f):
        return (size >= 24) and data.startswith('\211PNG\r\n\032\n') \
               and (data[12:16] == 'IHDR')

    def read_size(self, data, f):
        return struct.unpack(">LL", data[16:24])


class OldPng(AbstractImageFormat):
    def match(self, data, size, f):
        return (size >= 16) and data.startswith('\211PNG\r\n\032\n')

    def read_size(self, data, f):
        return struct.unpack(">LL", data[8:16])


class Gif(AbstractImageFormat):
    def match(self, data, size, f):
        return (size >= 10) and data[:6] in ('GIF87a', 'GIF89a')

    def read_size(self, data, f):
        return struct.unpack("<HH", data[6:10])


class Jpeg(AbstractImageFormat):
    def match(self, data, size, f):
        return (size >= 2) and data.startswith('\377\330')

    def read_size(self, data, f):
        msg = " raised while trying to decode as JPEG."
        f.seek(0)
        f.read(2)
        b = f.read(1)
        try:
            while b and ord(b) != 0xDA:
                while ord(b) != 0xFF:
                    b = f.read(1)
                while ord(b) == 0xFF:
                    b = f.read(1)

                if 0xC0 <= ord(b) <= 0xC3:
                    f.read(3)
                    h, w = struct.unpack(">HH", f.read(4))
                    return w, h
                else:
                    f.read(int(struct.unpack(">H", f.read(2))[0]) - 2)
                b = f.read(1)
        except struct.error:
            raise UnknownImageFormat("StructError" + msg)
        except ValueError:
            raise UnknownImageFormat("ValueError" + msg)
        except Exception as e:
            raise UnknownImageFormat(e.__class__.__name__ + msg)


class Pvr1and2(AbstractImageFormat):
    def match(self, data, size, f):
        f.seek(44)
        return struct.unpack('3s', f.read(3))[0] == "PVR"

    def read_size(self, data, f):
        """
        PVR HEADER SPECIFICATION
        Name                Offset  Size    Content
        Header Size         0       4       Integer Value
        Height              4       4       Integer Value
        Width               8       4       Integer Value
        Mip Map Count       12      4       Integer Value
        Pixel Format        16      1       Integer Value
        Flags               17      3       Bit Field
        Surface Size        20      4       Integer Value
        Bits Per Pixel      24      4       Integer Value
        Red Mask            28      4       Bit Field
        Green Mask          32      4       Bit Field
        Blue Mask           36      4       Bit Field
        Alpha Mask          40      4       Bit Field
        PVR Identifier 44   4       4       Characters
        Number of Surfaces  48      4       Integer Value
        """
        f.seek(0)
        f.read(4)
        h, w = struct.unpack("II", f.read(8))
        return w, h


class Pvr3(AbstractImageFormat):
    def match(self, data, size, f):
        f.seek(0)
        version = struct.unpack('<I', f.read(4))[0]
        return version in (0x50565203, 0x03525650)

    def read_size(self, data, f):
        """
        PVRv3 HEADER SPECIFICATION
        struct PVRv3Header {
            uint32_t version;
            uint32_t flags;
            uint64_t pixelFormat;
            uint32_t colorSpace;
            uint32_t channelType;
            uint32_t height;
            uint32_t width;
            uint32_t depth;
            uint32_t surfaceCount;
            uint32_t faceCount;
            uint32_t mipmapCount;
            uint32_t metadataLength;
        };
        """
        f.seek(0)
        f.read(24)
        h, w = struct.unpack("II", f.read(8))
        return w, h


class Pkm(AbstractImageFormat):
    def match(self, data, size, f):
        f.seek(0)
        return struct.unpack('3s', f.read(3))[0] == "PKM"

    def read_size(self, data, f):
        """
        HEADER SPECIFICATION
        Pos	Field	    Type	Size	Description
        0	Signature	char	3	    Constant string "PKM" (with NO size delimitation '\0' or so...)
        3	Version	    byte	1	    For the moment, it can take only the value 0. Other packing methods may change this field but there is only one for now...
        4	Pack_byte	byte	1	    Value of the recognition byte for color repetitions that are coded on 1 byte. (See the picture packing section for a better explanation)
        5	Pack_word	byte	1	    Value of the recognition byte for color repetitions that are coded on 2 bytes. (See the picture packing section...)
        6	Width	    word	2	    Picture width (in pixels)
        8	Height	    word	2	    Picture height (in pixels)
        10	Palette	    byte	768
           RGB palette (RGB RGB ... 256 times) with values from 0 to 63. I know the standard in picture files is 0 to 255 but I find it stupid! It is really easier to send the whole palette in port 3C9h with a REP OUTSB without palette convertion.
        778	PH_size	    word	2	    Post-header size. This is the number of bytes between the header and the picture data. This value can be equal to 0.

        Если делать по этой спецификации, выходит лажа.
        похоже, что перед длиной и шириной еще 2 магических байта,
        тогда похоже на правду. поэтому считываем 8, а не 6 байт
        """
        f.seek(0)
        f.read(8)
        w, h = struct.unpack(">HH", f.read(4))
        return w, h


class WebpVP8(AbstractImageFormat):
    def match(self, data, size, f):
        """
        https://developers.google.com/speed/webp/docs/riff_container
         0                   1                   2                   3
         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |      'R'      |      'I'      |      'F'      |      'F'      |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                           File Size                           |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |      'W'      |      'E'      |      'B'      |      'P'      |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        """
        f.seek(0)
        if struct.unpack('4s', f.read(4))[0] != "RIFF":
            return False
        f.read(4)
        if struct.unpack('4s', f.read(4))[0] != "WEBP":
            return False

        return struct.unpack('4s', f.read(4))[0] == "VP8 "

    def read_size(self, data, f):
        """
        HEADER SPECIFICATION
        // RIFF layout is:
        //   Offset  tag
        //   0...3   "RIFF" 4-byte tag
        //   4...7   size of image data (including metadata) starting at offset 8
        //   8...11  "WEBP"   our form-type signature
        // The RIFF container (12 bytes) is followed by appropriate chunks:
        //   12..15  "VP8 ": 4-bytes tags, signaling the use of VP8 video format
        //   16..19  size of the raw VP8 image data, starting at offset 20
        //   20....  the VP8 bytes
        // Or,
        //   12..15  "VP8L": 4-bytes tags, signaling the use of VP8L lossless format
        //   16..19  size of the raw VP8L image data, starting at offset 20
        //   20....  the VP8L bytes
        // Or,
        //   12..15  "VP8X": 4-bytes tags, describing the extended-VP8 chunk.
        //   16..19  size of the VP8X chunk starting at offset 20.
        //   20..23  VP8X flags bit-map corresponding to the chunk-types present.
        //   24..26  Width of the Canvas Image.
        //   27..29  Height of the Canvas Image.
        // There can be extra chunks after the "VP8X" chunk (ICCP, FRGM, ANMF, VP8,
        // VP8L, XMP, EXIF  ...)
        // All sizes are in little-endian order.
        // Note: chunk data size must be padded to multiple of 2 when written.

        """
        f.seek(0)
        f.read(20)  # header
        data = [struct.unpack("B", f.read(1))[0] for _ in range(10)]
        w = ((data[7] << 8) | data[6]) & 0x3fff
        h = ((data[9] << 8) | data[8]) & 0x3fff

        return w, h


class WebpVP8X(AbstractImageFormat):
    def match(self, data, size, f):
        """
        https://developers.google.com/speed/webp/docs/riff_container
         0                   1                   2                   3
         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |      'R'      |      'I'      |      'F'      |      'F'      |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |                           File Size                           |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        |      'W'      |      'E'      |      'B'      |      'P'      |
        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
        """
        f.seek(0)
        if struct.unpack('4s', f.read(4))[0] != "RIFF":
            return False
        f.read(4)
        if struct.unpack('4s', f.read(4))[0] != "WEBP":
            return False

        return struct.unpack('4s', f.read(4))[0] == "VP8X"

    def read_size(self, data, f):
        """
        HEADER SPECIFICATION
        // RIFF layout is:
        //   Offset  tag
        //   0...3   "RIFF" 4-byte tag
        //   4...7   size of image data (including metadata) starting at offset 8
        //   8...11  "WEBP"   our form-type signature
        // The RIFF container (12 bytes) is followed by appropriate chunks:
        //   12..15  "VP8 ": 4-bytes tags, signaling the use of VP8 video format
        //   16..19  size of the raw VP8 image data, starting at offset 20
        //   20....  the VP8 bytes
        // Or,
        //   12..15  "VP8L": 4-bytes tags, signaling the use of VP8L lossless format
        //   16..19  size of the raw VP8L image data, starting at offset 20
        //   20....  the VP8L bytes
        // Or,
        //   12..15  "VP8X": 4-bytes tags, describing the extended-VP8 chunk.
        //   16..19  size of the VP8X chunk starting at offset 20.
        //   20..23  VP8X flags bit-map corresponding to the chunk-types present.
        //   24..26  Width of the Canvas Image.
        //   27..29  Height of the Canvas Image.
        // There can be extra chunks after the "VP8X" chunk (ICCP, FRGM, ANMF, VP8,
        // VP8L, XMP, EXIF  ...)
        // All sizes are in little-endian order.
        // Note: chunk data size must be padded to multiple of 2 when written.

        """
        f.seek(0)
        f.read(24)  # header

        data = struct.unpack("BBB", f.read(3))
        w = (data[0] << 0 | data[1] << 8 | data[2] << 16) + 1

        data = struct.unpack("BBB", f.read(3))
        h = (data[0] << 0 | data[1] << 8 | data[2] << 16) + 1

        return w, h


_FORMATS = {
    '.png': [Png(), OldPng()],
    '.jpeg': [Jpeg()],
    '.gif': [Gif()],
    '.pvr': [Pvr1and2(), Pvr3()],
    '.pkm': [Pkm()],
    '.webp': [WebpVP8X(), WebpVP8()],
}


def _get_image_size(full_path, f, ext):
    error_msg = "Unknown format: {fmt}:{path}".format(fmt=ext, path=full_path)
    if ext not in _FORMATS:
        raise UnknownImageFormat(error_msg)

    data = f.read(25)

    f.seek(0, 2)  # to end
    file_size = f.tell()
    f.seek(0)  # to begin

    for fmt in _FORMATS[ext]:
        if fmt.match(data, file_size, f):
            img_size = fmt.read_size(data, f)
            # print("get_image_size({fmt}) {path}: {img_size}".format(
            #     fmt=str(fmt.__class__), path=full_path, img_size=img_size))
            return map(int, img_size)

    log.error(error_msg)
    raise UnknownImageFormat(error_msg + " - does not matched")
    # return 0, 0


def ccz_decompress(fullpath):
    out = StringIO()
    """
    struct CCZHeader {
        uint8_t  sig[4];			// signature. Should be 'CCZ!' 4 bytes
        uint16_t compression_type;	// should be 0
        uint16_t version;			// should be 2
        uint32_t reserved;			// Reserverd for users.
        uint32_t len;				// size of the uncompressed file
    };
    """
    with open(fullpath) as f:
        f.read(16)  # read ccz header
        data = f.read()
        decompressed = zlib.decompress(data)
        out.write(decompressed)
        return out


def ccz_decompress_to_file(fullpath):
    dst_fullpath, _ = os.path.splitext(fullpath)
    with open(dst_fullpath, 'w') as out:
        data = ccz_decompress(fullpath)
        data.seek(0)
        shutil.copyfileobj(data, out)


def get_image_size(file_path):
    """
    Return (width, height) for a given img file content -
    no external dependencies
    """

    if file_path.endswith('.ccz'):
        f = ccz_decompress(file_path)
        file_path, _ = os.path.splitext(file_path)
    else:
        f = open(file_path)

    _, ext = os.path.splitext(file_path)
    size = _get_image_size(file_path, f, ext)

    f.close()
    return size


if __name__ == '__main__':
    ccz_decompress_to_file('')
