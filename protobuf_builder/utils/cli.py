from __future__ import print_function

from .log import setup_logger
from .progressbar_wrap import init_propressbar
from .system import add_env_var
from . import Enum

from argparse import ArgumentParser

_assume_yes = False


def create_parser(version=None, *args, **kwargs):
    import logging
    parser = ArgumentParser(*args, **kwargs)

    if version is not None:
        parser.add_argument('--version', action='version', version=version)

    parser.add_argument(
        '--color',
        choices=BoolOption.all(),
        default=BoolOption.YES,
        help="color printing")

    parser.add_argument(
        '--debug',
        action="store_const",
        help="print debug information",
        dest="log_level",
        const=logging.DEBUG,
        default=logging.INFO)

    parser.add_argument(
        '--verbose',
        action="store_const",
        help="print verbose information",
        dest="log_level",
        const=logging.INFO,
        default=logging.INFO)

    parser.add_argument(
        '--progressbar',
        choices=BoolOption.all(),
        default=BoolOption.YES,
        help="show progressbar")

    parser.add_argument(
        '--assume_yes',
        action='store_true',
        default=False,
        help='assume yes to all questions')

    return parser


def is_assume_yes():
    return _assume_yes


def parse_args(path, parser=None):
    global _assume_yes
    # todo use six.string_types
    assert isinstance(path, basestring), \
        "Bad path: string expected {}".format(path)

    if parser is None:
        parser = create_parser(version=None)

    args = vars(parser.parse_args())

    log_level = args.pop('log_level')
    color = args.pop('color') == BoolOption.YES
    progressbar = args.pop('progressbar') == BoolOption.YES
    _assume_yes = args.pop('assume_yes', False)

    init_propressbar(progressbar)
    setup_logger(path, log_level, color)

    add_system_envs()
    # os.pathsep + CFG.tools_path

    return args


def add_system_envs():
    for var in ('/opt/local/bin', '/usr/local/bin'):
        add_env_var(var)


class SoOption(Enum):
    SKIP = "skip"
    REBUILT = 'rebuilt'
    CURRENT = 'current'


class BoolOption(Enum):
    YES = 'yes'
    NO = 'no'

    @classmethod
    def is_yes(cls, option):
        return option == cls.YES


class BuildType(Enum):
    RELEASE = 'release'
    DEBUG = 'debug'
